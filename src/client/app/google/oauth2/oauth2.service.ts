import { Injectable, EventEmitter }    from '@angular/core';
import { Http } from '@angular/http';
import { GlobalData }  from '../../global.data';

import { Config } from '../../app.config';

@Injectable()
export class Oauth2Service {
    
    serverUrl: string = this.config.protocol 
                         + this.config.host 
                         + ':' 
                         + this.config.port;
    
    oauth2Url:string = null;
    
    constructor( private http: Http,
                 private config: Config,
                 private global: GlobalData ) { }
    
    getOauth2Url = function() {
        return new Promise(resolve => {
            if (!this.global.googleOauth2Url) {
                this.http.get(this.serverUrl+this.config.apiPaths.get_oauth2_url)
                .subscribe( (res:any) => { 
                    this.global.googleOauth2Url = res.json().oauth2_url;
                    resolve(this.global.googleOauth2Url)
                })
            } else resolve(this.oauth2Url);
        })      
    }
        
}