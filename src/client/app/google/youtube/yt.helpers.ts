import { Injectable }    from '@angular/core';
import { GlobalData }  from '../../global.data';

@Injectable()
export class YtHelpers {

    constructor( private global:GlobalData) {}

    createNewPlaylistItemsNote(ex:any) { 
        var note ='Chinese HSK ' + ex.src.hsk + ' vocabulary: ' + ex.src.s + ' ' + ex.src.p1 + ', ' + ex.ex.txt_s + ' http://hsk.tips'
        return note
    }

    defineSelection() {
        var selection = {
            wordsCounter: 0,
            exCounter: 0,
            ex: <any>[]
        };
        if (this.global.words && this.global.words.words) {
            this.global.words.words.forEach( (w:any) => {
                if (w.checked) {
                    selection.wordsCounter++;
                    w.ex.forEach( (e:any) => {
                        if (e.checked) {
                            selection.exCounter++;
                            var item = {
                                    ex:e,
                                    src: w
                                }
                            selection.ex.push(item)
                        }
                    })
                }
            })
        }

        this.global.selectedExamples = selection;

        return selection
    }

    clearSelection() {
        this.global.words.words.forEach( (w:any) => {
          w.checked = false;
          w.ex.forEach( (e:any) => {
            e.checked = false;
          })
        })
        
        this.global.selectedExamples = {
            ex: null,
            exCounter: null,
            wordsCounter: null
        }
    }
}