import { Injectable, EventEmitter }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { GlobalData }  from '../../global.data';
import { Config } from '../../app.config';
import { YtHelpers } from './yt.helpers';

@Injectable()
export class YoutubeService {
    
    isStopInsertPlaylistItem: boolean = false;
    
    private headers = new Headers({'Content-Type': 'application/json'});

    serverUrl: string = this.config.protocol 
                         + this.config.host 
                         + ':' 
                         + this.config.port;    

    eYtPlaylists: EventEmitter<any>;
    eYtPlaylistInserted: EventEmitter<any>;
    eYtPlaylistItemInserted: EventEmitter<any>;

    constructor( private http: Http,
                 private config: Config,
                 private global: GlobalData,
                 private ytHelpers: YtHelpers ) {

                     this.eYtPlaylists = new EventEmitter();
                     this.eYtPlaylistInserted = new EventEmitter();
                     this.eYtPlaylistItemInserted = new EventEmitter();
                 }
    
    getPlaylists(pageToken?:string):Promise<any> { 
        var getYoutubePlaylistsUrl = this.serverUrl + this.config.apiPaths.get_ytPlaylists;
        if (pageToken) {
            getYoutubePlaylistsUrl+='?pageToken='+pageToken;
        };
        return this.http.get(getYoutubePlaylistsUrl)
            .toPromise()
            .then(res => this.getPlaylistsResponseHandler(res, pageToken))
            .catch( error => console.log(error) )
    }
    getPlaylistsResponseHandler(res:any, pageToken?:string) {
        var response = res.json();
        if (response.err) { 
            //TODO error handler
        } else {
            if (pageToken) {
                var oldItems = this.global.ytPlaylists.playlists.items;
                this.global.ytPlaylists = response;                                       
                this.global.ytPlaylists.playlists.items = oldItems.concat(res.json().playlists.items);
            } else {
                this.global.ytPlaylists = response;
            }                
            this.eYtPlaylists.emit(this.global.ytPlaylists);  
        }
    }

    insertPlaylist( newPlaylist:{} ):Promise<any> {
        var body = JSON.stringify(newPlaylist);
        var insertPlaylistUrl = this.serverUrl + this.config.apiPaths.insert_ytPlaylist;
        return this.http.post(insertPlaylistUrl, body, {headers: this.headers})
            .toPromise()
            .then( res => this.insertPlaylistsResponseHandler(res))
            .catch( error => console.log(error) )
            
    }
    insertPlaylistsResponseHandler(res:any){
        var response = res.json()
        if (response.err) { 
            //TODO error handler
        } else {
            this.global.ytPlaylists.playlists.items.unshift(response.response);
            this.eYtPlaylistInserted.emit(response.response);
        }
    }

    insertOnePlaylistItem( opt:any ):Promise<any> {
        var insertOneItemUrl = this.serverUrl + this.config.apiPaths.insert_playlistItems;
        opt.body = JSON.stringify(opt.body)
        return new Promise( resolve => {
            this.http.put(insertOneItemUrl, opt.body, {headers: this.headers})
            .toPromise()
            .then (res => this.insertOnePlaylistItemResponseHandler(opt, res))
            .then ( () => resolve() )
            .catch( (error) => console.log(error) )
            
        })
    }
    
    insertOnePlaylistItemResponseHandler(opt:any, res:any) {
        return new Promise(resolve => {
            var response = res.json()
            var status = '';
            if (response.err) {
                status = 'Error! Unstable connection of you logged out.'
            } else { 
                this.eYtPlaylistItemInserted.emit(response.response)
                this.global.lastYtPlaylistLink = 'https://www.youtube.com/playlist?list=' + response.response.snippet.playlistId;
                status = 'Ok'
            }
            var forLog = opt.forLog
            var video = forLog.src.s + ', ' +forLog.src.p1 + ', '+ forLog.ex.txt_s;
            this.global.addItemsToPlaylistLog.push({video: video, status: status})

            resolve()
        })
    }

    insertManyPlaylistItems(playlist:any) {
        this.ytHelpers.defineSelection();
        this.global.addItemsToPlaylistLog = [];
        var self = this;
        var counter = 0;
        var videos = this.global.selectedExamples.ex;

        return new Promise( resolve => {
            if (this.global.selectedExamples.exCounter > 0) { //check is any selected
                this.global.progressBar.isShow = true;
                insertMany(resolve)
            } else resolve();
        });
        

        function insertMany(resolve:any) {            
            if (self.isStopInsertPlaylistItem == true) {
                self.onFinishInsertManyPlaylistItems();
                resolve();
            }
            var opt = { 
                    body: {
                        playlistId: playlist.id,
                        videoId: videos[counter].ex.ytid,
                        note: self.ytHelpers.createNewPlaylistItemsNote(videos[counter])
                    }, 
                    forLog: videos[counter]
                }     

            self.insertOnePlaylistItem(opt).then( () => {
                counter++
                self.global.progressBar.progressPersent = String(counter/self.global.selectedExamples.exCounter*100).substr(0,4)+"%";
                self.global.progressBar.progress = counter;
                
                if (counter < self.global.selectedExamples.exCounter) {
                    insertMany(resolve);
                } else  {
                    self.onFinishInsertManyPlaylistItems()
                    resolve();
                }
            })
        }
    }

    onFinishInsertManyPlaylistItems() {
      this.isStopInsertPlaylistItem = false;
      this.global.progressBar.finished = true;
      this.ytHelpers.clearSelection();
    }


} 