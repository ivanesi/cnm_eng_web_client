import { Injectable } from '@angular/core';

@Injectable()
export class NewYtItemDefaultData { 
    newYtPlaylist: {
        title: string,
        description: string,
        tags:Array<string>
    } = {
        title: ', www.hsk.tips',
        description: 'https://hsk.tips - Chinese HSK vocabulary and grammar tool',
        tags: [
            "hsk", 
			"chinese",
			"hsk chinese",
			"chinese hsk",
            "chinese vocabulary",
            "chinese character",
            "hsk tips",
            "hsk.tips",
			"hsk1",
			"hsk2", 
			"hsk3", 
			"hsk4", 
			"hsk5", 
			"hsk6",
			"hsk 1",
			"hsk 2", 
			"hsk 3", 
			"hsk 4", 
			"hsk 5", 
			"hsk 6",
			"vocabulary",
			"hsk vocabulary",
			"hsk words",
			"hsk writing",
			"hsk listening",
			"chinese lessons",
			"chinese lessons mandarin",
			"study chinese",
			"study mandarin",
			"learn mandarin",
			"learn chinese"
        ]
    }

}