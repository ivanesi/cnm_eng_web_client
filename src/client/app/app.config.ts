import { Injectable }    from '@angular/core';

@Injectable()
export class Config {

  // protocol: string = 'http://';
  // host: string = 'localhost';
  // port: string = '2999';

  protocol: string = 'https://';
  host: string = 'hsk.tips';
  port: string = '';

  serverUrl: string = this.protocol 
                    + this.host 
                    + ':' 
                    + this.port;
  
  apiPaths: {
    get_hsk_wordlists_basic_info: string,
    get_hsk_words: string,
    //google
    get_oauth2_url: string,
    get_ytPlaylists:string,
    insert_ytPlaylist: string,
    insert_playlistItems: string,
    //endof google
    user_logout: string,
    get_userinfo: string,
    //
    edit_word: string
    } = {
    get_hsk_wordlists_basic_info: '/api/cnm_eng/guest/get_hsk_wordlists_basic_info',
    get_hsk_words: '/api/cnm_eng/guest/get_hsk_words',
    //google
    get_oauth2_url:       '/api/gapi/oauth2/get_url',
    get_ytPlaylists:      '/api/gapi/youtube/playlists',
    insert_ytPlaylist: '/api/gapi/youtube/playlists/insert', 
    insert_playlistItems: '/api/gapi/youtube/playlistsItems/insert',
    //endof google
    user_logout: '/api/user/logout',   
    get_userinfo: '/api/user/userinfo', 
    //admin
    edit_word: '/adminka/edit_word/'
  }

  numberOfWordsOnPage: number = 25; //shoud be the same like on server limit in db req
  
}