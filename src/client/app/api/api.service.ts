import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { Config } from '../app.config';
import { GlobalData } from '../global.data';

@Injectable()
export class ApiService {
    public baseReqWordsUrl: string;
    
    public eUserinfo : EventEmitter<any>;
    public eGetWords: EventEmitter<any>;
    public eWordsGotten : EventEmitter<any>;
  
    private timeoutId:any;

    constructor( private http: Http, 
                 private config: Config, 
                 private global: GlobalData) {
                    this.eWordsGotten = new EventEmitter();
                    this.eGetWords = new EventEmitter();
                    this.eUserinfo = new EventEmitter();
                 }
                 
    getUserinfo() {
        var url: string = this.config.serverUrl + this.config.apiPaths.get_userinfo;
        return new Promise (resolve => {
            this.http.get(url).subscribe( (res:any) => {
                this.global.userinfo = res.json();
                this.eUserinfo.emit(this.global.userinfo )
                resolve(this.global.userinfo);
            })
        })
    }

    getHskLevels() {
        var url: string = this.config.serverUrl + this.config.apiPaths.get_hsk_wordlists_basic_info;
        return new Promise (resolve => {
            if (this.global.hskLevels == null) {
                this.http.get(url).subscribe( (res:any) => {
                    this.global.hskLevels = res.json();
                    resolve(this.global.hskLevels)
                });
            } else { resolve(this.global.hskLevels) }
        });  
    }

    getWords(opt:any) {
        this.global.isLoading = true;
        this.eGetWords.emit(true)
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout( () => { this._getWords(opt) }, 1000)
    }
    
    _getWords(opt:any) {
        if (!opt.url) {
            var reqParams = this.createReqParamsForWord(opt);
            opt.url = this.config.serverUrl + this.config.apiPaths.get_hsk_words+reqParams;
            this.baseReqWordsUrl = opt.url;
        }
        return new Promise (resolve => {
                this.http.get(opt.url).subscribe( (res:any) => {
                    this.global.words = res.json();
                    this.eWordsGotten.emit({ words:this.global.words, opt});
                    resolve(this.global.words)
                });
        });
    }
    
    createReqParamsForWord(opt:any) {
            var paramsString = '?';
            this.global.hskLevels.forEach( (level:any) => {
                if (level.checked) {
                    paramsString += 'l='+level.id+'&'
                }
            });
            if (opt.searchFor) {
                paramsString += 'w='+opt.searchFor+"&"
            }
            if (opt.wordlist) {
                paramsString +='wl='+opt.wordlist+'&'
            }
            return paramsString
    }
    
    editWord(word:any) {
        var url: string = this.config.serverUrl + this.config.apiPaths.edit_word;
        window.open(url+word._id);
    }
}