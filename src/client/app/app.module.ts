import { APP_BASE_HREF } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';

import { NgModule }      from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { RouterModule, Routes } from '@angular/router';

import { Config } from './app.config';
import { GlobalData } from './global.data';
import { ApiService } from './api/api.service';
import { Oauth2Service } from './google/oauth2/oauth2.service';
import { YoutubeService } from './google/youtube/youtube.service';
import { NewYtItemDefaultData } from './google/youtube/newYtItemDefaultData';
import { YtHelpers } from './google/youtube/yt.helpers';

//reusable components

import { MDLUpgradeElementDirective } from './components/MaterialDesignLiteUpgradeElement';

import { DragBtn } from './components/dragBtn/dragBtn.component';
import { WordlistsComponent } from './components/wordlists/wordlists.component';
import { HskLevelsCheckboxesComponent } from './components/hskLevelsChekboxes/hskLevelsCheckboxes.component';
import { GOauth2BtnComponent } from './components/google/oauth2/oauth2Btn_component/gOatuh2Btn.component';
import { getUserPlaylistsBtnComponent } from './components/google/youtube/playlists/getUserPlaylistsBtn/getUserPlaylistsBtn.component';
import { YtPlFixRightComponent } from './components/google/youtube/playlists/list-fixed-right/ytPlFixRight.component';
import { HeaderComponent } from './components/header/header.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { PopupComponent } from './components/popup/popup.component';
import { ProgressBarComponent } from './components/progressBar/progressBar.component';

//pages
import { RootComponent} from './root/root.component';
import { HomeComponent }  from './pages/home/home.component';
import { VocabularyComponent } from './pages/vocabulary/vocabulary.component';

import { PageNotFoundComponent } from './pages/pageNotFound/pageNotFound.component';

@NgModule({
 declarations: [
    MDLUpgradeElementDirective,
    RootComponent,
    GOauth2BtnComponent,
    getUserPlaylistsBtnComponent,
    YtPlFixRightComponent,
    HeaderComponent,
    HskLevelsCheckboxesComponent,
    HomeComponent,
    VocabularyComponent,
    PaginationComponent,
    PopupComponent,
    ProgressBarComponent,
    WordlistsComponent,
    DragBtn,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [Config, GlobalData, ApiService, Oauth2Service, YoutubeService, 
              NewYtItemDefaultData, YtHelpers, {provide: APP_BASE_HREF, useValue: '<%= APP_BASE %>' } ],
  bootstrap: [RootComponent]
})

export class AppModule { }