import { Component } from '@angular/core';
import { Config } from '../../app.config';
import { Oauth2Service } from '../../google/oauth2/oauth2.service';
import { GlobalData } from '../../global.data';
import { ApiService } from '../../api/api.service';

function getWindow(): any {
    return window;
}

@Component({
  selector: 'header-app',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.css']
})

export class HeaderComponent {
    needGAuth = true;
    constructor ( private config: Config,
                  private oauth2Service : Oauth2Service,
                  private api: ApiService,
                  public global: GlobalData) {
                      this.api.eUserinfo.subscribe( (user:any) => this.onUserinfo(user))
                  }
    //site menu
    goToOurYoutubePlaylists() {
        window.open('https://www.youtube.com/channel/UCaUWFF0waodhhDb5zwy8iQQ/playlists');
    }

    writeUs() {
        var email = "hsk.tips@gmail.com";
        var subject = "hsk.tips feedback";
        var url = 'https://mail.google.com/mail/u/0/?ui=2&view=cm&fs=1&tf=1&to='
                  + email
                  +'&su='
                  + subject;
        window.open(url);
    }
    
    //user menu    
    onUserinfo(user:any) {
        if (this.global.userinfo && this.global.userinfo.userinfo) this.needGAuth = false;
    }
  
    googleLogOut(google?:string) {
        var window = getWindow();
        var currentUrl = window.location.href;
        var params = '?redirectUrl='+currentUrl;
        if (google) { params+="&google=true"}
        window.location.href = this.config.serverUrl+this.config.apiPaths.user_logout+params   
    }

    goToYoutubePlaylists() {
        window.open('https://youtube.com/view_all_playlists')
    }
    
    showPlaylistsList(event:any) {
        this.global.isShowYtPlaylistsList = true
    }
}