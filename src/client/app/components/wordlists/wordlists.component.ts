import { Component, Output, EventEmitter } from '@angular/core';
import { GlobalData } from '../../global.data';
import { ApiService } from '../../api/api.service';

@Component({
  selector: 'wordlists',
  templateUrl: 'wordlists.component.html',
  styleUrls: ['wordlists.component.css']
})

export class WordlistsComponent {
  @Output() eWordlistChanged = new EventEmitter();
  showNumber:number = 5;
  isShowAll: boolean = false;

  constructor( public global:GlobalData,
               private api: ApiService ) {}


  showAllWordlists() {
    this.isShowAll = true;
  }

  hideWordlists() {
    this.isShowAll = false;
  }

  getWords(wl:any) {
    this.global.selectedExamples.wordsCounter = 0;
    this.api.getWords( {wordlist: wl.name} )
    this.global.currentWordlist = wl.name
    this.eWordlistChanged.emit(true);
  }
  
}
