import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../../api/api.service';

import { GlobalData } from '../../global.data';

@Component({
  selector: 'hskLevels-chbx',
  styles: [`
            .hskLevels-chbx {
                margin:10px;
            }
            label {
                display:inline-block;
                width: 80px;
            }
            .level-label {
                font-size: 1.3em;
            }
            `],
  template: `<div class="hskLevels-chbx">
                <div class="loading-container">
                    <div *ngIf="isLoading" class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>
                </div>
             <span *ngFor="let level of global.hskLevels; let i = index">
                <label 
                    for="level-checkbox-{{i}}"
                    class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect">
                    <input type="checkbox" 
                        id="level-checkbox-{{i}}" 
                        class="mdl-checkbox__input"
                        (change)='selectHskLevel(level, $event, i)'
                        [checked]="level.checked">

                    <span class="mdl-checkbox__label level-label">{{level.hskName }} </span>
                </label> 
            </span>
            </div>
            `
})
export class HskLevelsCheckboxesComponent implements OnInit  {
    
    @Output() eHskLevelChanged = new EventEmitter();
    
    isLoading = true;
    
    constructor( public global: GlobalData,
                 private api: ApiService ) {}

    ngOnInit(): void {
        this.api.getHskLevels().then((levels:any) => this.isLoading = false)
    }

    
    selectHskLevel(level:any, event:any, i:any) {
      this.global.selectedExamples.wordsCounter = 0;
      this.global.hskLevels[i].checked = event.currentTarget.checked;
      this.eHskLevelChanged.emit(level);
      this.api.getWords({});
      this.global.defineWordlists();
    }
}