import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { GlobalData } from '../../global.data';

@Component({
  selector: 'pagination-app',
  templateUrl: 'pagination.component.html',
  styleUrls: ['pagination.component.css']
})

export class PaginationComponent implements OnInit, OnChanges{
  
  @Input() currentPage: any;
  @Input() numberOfPages: number;
  @Input() pageLinks: string;

  @Output() eCurrentPage = new EventEmitter(); 
  @Output() eReqPageContent = new EventEmitter();
  
  sliceTo:number=8;
  currentPageClassStatus: Array<boolean> = [];

  ngOnChanges(changes: any) {
    if (changes.currentPage) this.onNewCurrentPage(changes.currentPage);
    if (changes.numberOfPages) this.onNewNumberOfPages(changes.numberOfPages.currentValue);
    if (changes.pageLinks) this.OnNewPageLinks(changes.pageLinks);
  }

  constructor( private globals: GlobalData) {}

  ngOnInit() {
    this.currentPage = 1;
  }

  OnNewPageLinks(pageLinks:any){
    this.pageLinks = pageLinks.currentValue;
    this.sliceTo = 8;
  }

  onNewCurrentPage(newCurrentPage:any) {
    this.currentPageClassStatus[newCurrentPage.currentValue-1] = true;
    this.currentPageClassStatus[newCurrentPage.previousValue-1] = false;
    this.currentPage = newCurrentPage.currentValue;
  }

  onNewNumberOfPages(newNumber:any) {
    this.numberOfPages = newNumber;    
  }
  gotoPage(i:number) {
    if (i+1 != this.currentPage) {
      this.eReqPageContent.emit(this.pageLinks[i])
      this.currentPage = i+1;
      this.eCurrentPage.emit(this.currentPage);
    }
  }
  toNextPage() {
    if (this.currentPage < this.numberOfPages) {
      if (this.currentPage < this.numberOfPages-2 && this.currentPage >= this.sliceTo-2) {
        this.sliceTo +=2;
      }
      this.currentPage += 1;
      this.eReqPageContent.emit(this.pageLinks[this.currentPage-1])
      this.eCurrentPage.emit(this.currentPage);
    }
  }
  toPrevPage() {
    
     if (this.currentPage > 1) {
      if (this.currentPage <= this.sliceTo-5 && this.sliceTo > 8) {
        this.sliceTo -=1;
      }
      //if (this.currentPage == 2 ) this.sliceTo = 10
      this.currentPage -= 1;
      this.eReqPageContent.emit(this.pageLinks[this.currentPage-1]);
      this.eCurrentPage.emit(this.currentPage);
    }
  }
  onInputPageNumber(event:any) {
    if (event.keyCode == 13) {
      var value = Number(event.target.value)
      if (value <= this.numberOfPages && value > 0) {
        this.currentPage = value;

        this.eReqPageContent.emit(this.pageLinks[this.currentPage-1]);
        this.eCurrentPage.emit(this.currentPage);

        if (this.currentPage > this.sliceTo) {
          if ( this.currentPage%2 == 0) {
            this.sliceTo = this.currentPage + 2;
          } else { 
            this.sliceTo = this.currentPage + 3;
          }
        }
        if (this.currentPage < this.sliceTo-5) {
          this.sliceTo = this.currentPage + 7;
        }
      } 
      event.target.value = '';
    }
  }
}
