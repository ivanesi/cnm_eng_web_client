import { Component, AfterViewInit, Output, EventEmitter } from '@angular/core';

declare var Draggabilly: any;

function _dragBtn_getWindow(): any {
    return window;
}

@Component({
    selector: 'drag-btn',
    templateUrl: 'dragBtn.html',
    styleUrls: ['dragBtn.css'],
})

export class DragBtn implements AfterViewInit {
    
    @Output() eClick= new EventEmitter();

    window:any;

    dragBtn: any;
    dummy:any;

    btnPosLeft:any;
    btnPosTop:any;
    
    ngAfterViewInit() {
        this.setDrag()
    }

    setDrag() {
        this.window = _dragBtn_getWindow();
        this.dragBtn = new Draggabilly(window.document.querySelector('.draggable'),{})
        this.dummy = new Draggabilly(window.document.querySelector('.dummy'),{})
        this.dummy.disable()
        
        this.setDragBtnPosition(null)
        
        this.dragBtn.on( 'staticClick', () => {
            this.onClick()
        })
    }

    setDragBtnPosition(event?:any) {
        this.btnPosLeft = this.dummy.element.offsetLeft +'px';
        if (event) this.btnPosTop = event.target.innerHeight-100+'px';
    }
    
   
    onClick() {
        this.eClick.emit();
    }

    
}