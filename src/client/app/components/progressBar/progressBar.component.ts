import { Component, Input, Output, EventEmitter } from '@angular/core';
import { GlobalData } from '../../global.data';

@Component({
  selector: 'progress-bar',
  templateUrl: 'progressBar.component.html',
  styleUrls: ['progressBar.component.css']
})

export class ProgressBarComponent {

  @Input() total: number;
  @Input() progress: number;
  @Input() progressPersent: string;
  @Input() isShowProgressBar: boolean;    

  @Output() eStop = new EventEmitter();
  
  constructor( public global:GlobalData ) {}

  stop() {
    this.eStop.emit(true)
  }

  ok() {
    this.global.progressBar.isShow = false;
    this.global.progressBar.finished = false;
    this.global.progressBar.progress = 0;
    this.global.progressBar.progressPersent = '0%'    
  }
  
}
