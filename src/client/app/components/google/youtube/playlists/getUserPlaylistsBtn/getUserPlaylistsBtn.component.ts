import { Component } from '@angular/core';
import { YoutubeService } from '../../../../../google/youtube/youtube.service';

@Component({
  selector: 'getYtPlst-btn',
  styles:[`
            .gauth-btn { cursor: pointer; }
            .mdl-navigation__link { color: white;}
            .mdl-navigation__link:hover {color: darkblue;}
        `],
  template: `<span class="gauth-btn mdl-navigation__link" 
                (click)=getPlaylists()> Add to Playlist </span>`
})
export class getUserPlaylistsBtnComponent {
    
    constructor( private yt: YoutubeService ) {}

    getPlaylists() {
        this.yt.getPlaylists();
    }
}
    