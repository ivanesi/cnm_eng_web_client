import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { YoutubeService } from '../../../../../google/youtube/youtube.service';
import { NewYtItemDefaultData } from '../../../../../google/youtube/newYtItemDefaultData';
import { YtHelpers } from '../../../../../google/youtube/yt.helpers';
import { GlobalData } from '../../../../../global.data';

@Component({
  selector: 'ytpl-fix-right',
  templateUrl: 'ytPlFixRight.component.html',
  styleUrls: ['ytPlFixRight.component.css']
})

export class YtPlFixRightComponent {
  
  @Input() isShowList: boolean = false;
  @Output() eIsShowList = new EventEmitter(); 

  isLoading: boolean = true;
  isAddNewPlLoading: boolean = false;

  newPlaylist:{ title:string } = { title: '' };

  playlists: {};

  constructor( private yt: YoutubeService,
               public global: GlobalData,
               private ytDefaults: NewYtItemDefaultData,
               private ytHelpers: YtHelpers) {

     yt.eYtPlaylists.subscribe( (playlists:any) => this.onPlaylists(playlists)) 
     yt.eYtPlaylistInserted.subscribe( (playlist:any) => this.onPlaylistInserted(playlist));
  }


  createNewPlaylist() {
    if ( this.newPlaylist.title.length > 0) {
      this.isAddNewPlLoading = true;

      var newPlaylist = {
        title: this.newPlaylist.title + this.ytDefaults.newYtPlaylist.title,
        description: this.ytDefaults.newYtPlaylist.description,
        tags:  this.ytDefaults.newYtPlaylist.tags
      }
      
      this.newPlaylist.title = '';
      
      this.yt.insertPlaylist(newPlaylist);
    }
  }

  onPlaylistInserted(playlist:any) {
    this.isAddNewPlLoading = false;
    this.playlists = this.global.ytPlaylists.playlists;
  }

  onKeyAddPlInput(event:any) {
    if (event.keyCode == '27' ) {
      this.newPlaylist.title = ''
    }
    if (event.keyCode == '13' ) {
      this.createNewPlaylist();
    }
  }
  
  onPlaylists(playlists:any) {
    this.playlists = this.global.ytPlaylists.playlists
    this.isLoading = false;
  }

  addVideosToPlaylist(playlist:any) {
    this.global.isShowYtPlaylistsList = false;
    this.yt.insertManyPlaylistItems(playlist)
  }  
  
  closeList() {
    this.isShowList = false;
    this.eIsShowList.emit(false);
  }

  loadMorePlaylists() {
    this.isLoading = true;
    var pageToken = this.global.ytPlaylists.playlists.nextPageToken;
    this.yt.getPlaylists(pageToken);
  }
}
