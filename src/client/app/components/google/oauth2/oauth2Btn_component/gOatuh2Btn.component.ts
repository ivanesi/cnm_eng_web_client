import { Component, OnInit } from '@angular/core';
import { Oauth2Service } from '../../../../google/oauth2/oauth2.service';
import { GlobalData } from '../../../../global.data';
import { ApiService } from '../../../../api/api.service';

function getWindow(): any {
    return window;
}

@Component({
  selector: 'gOauth2-btn',
  styles:[`
            .gauth-btn { cursor: pointer; }
            .mdl-navigation__link { color: white;}
            .mdl-navigation__link:hover {color: darkblue;}
        `],
  template: `<span class="gauth-btn mdl-navigation__link" 
                (click)=googleLogIn() 
                *ngIf="!loading" > Google LogIn</span>
            <div *ngIf="loading" 
                class="mdl-spinner mdl-js-spinner is-active" 
                style="color: white; width: 20px; height:20px;"></div>
                `
})
export class GOauth2BtnComponent implements OnInit  {
    
    window: any;
    currentOauth2Url: string;
    
    loading:boolean = true;
    
    constructor(private oauth2Service: Oauth2Service,
                private api: ApiService,
                private global: GlobalData ){}

    ngOnInit(): void {
        this.window = getWindow();
        this.setCurrentOauth2Url();
        this.api.getUserinfo().then( (data:any) => this.onUserinfo(data) );
    }

    googleLogIn() {
        this.loading = true;
        this.setCurrentOauth2Url()
        this.window.location.href = this.currentOauth2Url;
    };

    onUserinfo(data:any) {
        this.loading = false;
    }

    setCurrentOauth2Url() {
        var currentUrl = this.window.location.href;
        if (!this.global.googleOauth2Url) { 
                this.oauth2Service.getOauth2Url().then( (url:any) => {
                    this.currentOauth2Url = url + '&state=' + currentUrl;
            });
        } else {
            this.currentOauth2Url = this.global.googleOauth2Url + '&state=' + currentUrl;
        }
    }

}