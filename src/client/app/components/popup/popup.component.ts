import { Component, Input } from '@angular/core';


@Component({
  selector: 'popup-app',
  templateUrl: 'popup.component.html',
  styleUrls: ['popup.component.css']
})

export class PopupComponent {

  @Input() opt: {
      isShow: boolean,
      msg: string,
      gauthBtn: boolean,
      ytPlstBtn: boolean,
    };
         
}
