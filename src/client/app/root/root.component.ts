declare let componentHandler: any; //for material design light

import { Component, AfterViewInit} from '@angular/core';

import { GlobalData } from '../global.data';
  
@Component({
    selector: 'root-app',
    styleUrls: ['root.component.css'],
    template: `<div mdl id="rootContainer" class="mdl-layout mdl-js-layout">
                    <header-app> </header-app>
                    <ytpl-fix-right [isShowList]="global.isShowYtPlaylistsList"
                                    (eIsShowList)=showYtPlaylistsList($event)> 
                    </ytpl-fix-right>
                    <div class="content-wrapper">
                        <router-outlet></router-outlet>
                    </div>
               </div>
               `
})
export class RootComponent implements AfterViewInit { 
    ngAfterViewInit() { //for material design light
        componentHandler.upgradeDom(); // upgrade all mdl components
    }
    constructor( public global: GlobalData) {}
    
    showYtPlaylistsList(event:any) {
        this.global.isShowYtPlaylistsList = event;
    }
}
