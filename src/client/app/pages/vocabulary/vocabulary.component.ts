import { Popup } from './services/popup';
import { SelectWords } from './services/selectWords';
import { Helpers } from './services/helpers';
import { Vars } from './services/vars';
import { Pagination } from './services/pagination';
import { Search } from './services/search';
import { Playlists } from './services/playlists';

import { DragBtn} from '../../components/dragBtn/dragBtn.component';

import { Component, OnInit, ViewChild  } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { ApiService } from '../../api/api.service';
import { GlobalData } from '../../global.data';

import { PaginationComponent } from '../../components/pagination/pagination.component';

@Component({
    selector: 'voc-app',
    templateUrl: 'vocabulary.component.html',
    styleUrls: ['vocabulary.component.css'],
    providers: [Popup, SelectWords, Helpers, Pagination, Search, Vars, Playlists]
})
export class VocabularyComponent implements OnInit { 
    
    constructor( public global: GlobalData,
                 private api: ApiService,
                 public popup: Popup,
                 public select: SelectWords,
                 public pagination: Pagination,
                 public search: Search,
                 private helpers: Helpers,
                 public vars: Vars, 
                 public playlists: Playlists) {

        api.eGetWords.subscribe( (tmp:any) => this.global.isLoading = true );
        api.eWordsGotten.subscribe( (data: any) => this.onWordsGotten(data) );

    }
    
    ngOnInit() {
        
        if ( this.global.words && this.global.words.total > 0 ) { 
            this.pagination.setDataForPagination() 
        } 
        
    }    
    
    editWord(word:any) {
        this.api.editWord(word);
    }

    onWordsGotten(data:any) {
        this.global.isLoading = false;
        if (!data.opt.dontUpdPagination) this.pagination.setDataForPagination()
    }

    hskLevelChanged(event:any) { //for hskLevelCheckboxes (see html)
        this.search.searchWordInputValue = '';
        this.global.isLoading = true;
        this.global.currentWordlist = null;
        this.global.selectedExamples.wordsCounter = 0;
        this.select.isSelectAll = true;
        this.search.searchWordInputValue = '';
    }    

    showExMeaning(wIndx:number, exIndx:number) {
        if (!this.global.words.words[wIndx].ex[exIndx].isShowMean) {
            this.global.words.words[wIndx].ex[exIndx].isShowMean = true;
        } else {
            this.global.words.words[wIndx].ex[exIndx].isShowMean = false;
        }
    }   

    clearCurrentWordlist() {
        this.search.clearSearchWordInputValue();
        this.global.selectedExamples.wordsCounter = 0;
        this.global.currentWordlist = null;
        this.api.getWords({})
    } 


   isShowDragBtn() {
        if ( (this.global.selectedExamples.wordsCounter > 0) 
              && !this.global.isShowYtPlaylistsList
              && !this.global.progressBar.isShow) {
            return 'visible'
        } else {
            return 'hidden'
        }
    }
    
}