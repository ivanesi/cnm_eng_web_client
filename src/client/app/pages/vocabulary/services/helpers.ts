import { Injectable } from '@angular/core';

import { GlobalData } from '../../../global.data';
import { Popup } from './popup';
import { Vars } from './vars';

@Injectable()
export class Helpers {

    constructor( private global: GlobalData,
                 private popup: Popup,
                 private vars: Vars) {}

    checkIfNeedGauth() {
        if (this.global.userinfo.userinfo ) {
            this.popup.opt.isShow = false;
            this.vars.isNeedGauth = false;
            return false
        } else {
            this.popup.showPopup({ gauthBtn: true})
            this.vars.isNeedGauth = true;
            return true
        }
    }
}