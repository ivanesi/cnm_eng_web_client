import { Injectable } from '@angular/core';
import { YoutubeService } from '../../../google/youtube/youtube.service';
import { Vars } from './vars';
import { GlobalData } from '../../../global.data';

@Injectable()
export class Playlists {
        
    constructor (private yt:YoutubeService,
                 private vars: Vars,
                 private global: GlobalData) {

        this.yt.eYtPlaylists.subscribe( (data:any) => this.onYtPlaylists() )
    }
    
    getPlaylists() {
        this.global.isLoading = true;
        this.yt.getPlaylists();
    }

    onYtPlaylists() {
        this.global.isShowYtPlaylistsList = true;
        this.global.isLoading = false;
    }
    
    onStopAddVideoToPlaylist(event:boolean) {
        this.yt.isStopInsertPlaylistItem = true;
    }
}