import { Injectable } from '@angular/core';

import { GlobalData } from '../../../global.data';
import { SelectWords } from './selectWords';
import { Vars } from './vars';
import { ApiService } from '../../../api/api.service';
import { Config } from '../../../app.config';

@Injectable()
export class Pagination {
    
    numberOfWordsOnPage: number = this.config.numberOfWordsOnPage;

    currentPage:number;
    numberOfPages:number;
    pageLinks:Array<string> = [];

    constructor( private vars: Vars,
                 private global: GlobalData,
                 private select: SelectWords,
                 private config: Config,
                 private api: ApiService) {}

    newCurrentPage(currentPage:any) {
        this.global.isLoading = true;
        this.currentPage = currentPage;
        this.global.selectedExamples.wordsCounter = 0;
        this.select.isSelectAll = true;
    }
    
    reqPageContent(url: any) {
        this.api.getWords({url:url, dontUpdPagination: true});
    }

    setDataForPagination() {
        this.currentPage = 1;
        this.numberOfPages = Math.ceil(this.global.words.total/this.numberOfWordsOnPage);
        //create pageLinks
        this.pageLinks = [];
        for (var i = 0; i < this.numberOfPages; i++) {
            this.pageLinks.push(this.api.baseReqWordsUrl+'skip='+i*this.numberOfWordsOnPage)
        }
    }
}