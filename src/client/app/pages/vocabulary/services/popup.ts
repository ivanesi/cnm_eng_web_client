import { Injectable } from '@angular/core';

@Injectable()
export class Popup {
     opt: {
      isShow: boolean,
      msg: string,
      gauthBtn: boolean,
      ytPlstBtn: boolean
    } = {
      isShow: false,
      msg: '',
      gauthBtn: false,
      ytPlstBtn: false
    };

    showPopup(opt:any) {
        if (opt.ytPlstBtn) {
            opt.msg ="You can add words to Youtube playlist"
        }
        if (opt.gauthBtn) {
            opt.msg = "Please logIn with google"
        }
        this.opt = {
            isShow: true,
            msg: opt.msg,
            gauthBtn: opt.gauthBtn,
            ytPlstBtn: opt.ytPlstBtn
        };
    }
}