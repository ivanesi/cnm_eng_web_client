import { Injectable } from '@angular/core';

import { GlobalData } from '../../../global.data';
import { Popup } from './popup';
import { Helpers } from './helpers';
import { Playlists } from './playlists';

@Injectable()
export class SelectWords {

    isSelectAll: boolean = true;

    constructor(private global:GlobalData,
                private popup: Popup,
                private helpers: Helpers,
                private playlists: Playlists) {}

    selectWord(word:any, event:any) {
        if( !this.helpers.checkIfNeedGauth() ) {
            word.checked = event.target.checked;
            word.ex.forEach( (ex:any) => {
                ex.checked = event.target.checked;
            })
            this.countSelectedWords();
        }
    }

    selectExample(word:any, ex:any, event:any) {
        if( !this.helpers.checkIfNeedGauth() ) {
            ex.checked = event.target.checked
            if (ex.checked) { 
                word.checked = true;
            } else {
                var isWordChecked = false;
                word.ex.forEach( (e:any) => {
                    if (e.checked) isWordChecked = true;
                    return 
                })
                word.checked = isWordChecked;
            }
            this.countSelectedWords();
        }
    }

    selectAll(flag:any) {
        
        if (!this.helpers.checkIfNeedGauth() && this.global.words.words) {
            if (flag) {
                this.global.words.words.forEach( (w) => {
                    w.checked = true;
                    w.ex.forEach( (e:any) => e.checked = true)
            })
                this.isSelectAll = false;
            } else {
                this.global.words.words.forEach( (w) => {
                    w.checked = false;
                    w.ex.forEach( (e:any) => e.checked = false)
                });
                this.isSelectAll = true;
            }
            this.countSelectedWords()
        }
    }
    countSelectedWords() {
        var counter = 0;
        this.global.words.words.forEach( (w) => {
            if (w.checked == true) counter++
        })
        this.global.selectedExamples.wordsCounter = counter;
    }
}