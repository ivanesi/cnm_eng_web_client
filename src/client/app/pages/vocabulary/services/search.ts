import { Injectable } from '@angular/core';

import { GlobalData } from '../../../global.data';
import { ApiService } from '../../../api/api.service';
import { Popup } from './popup';
import { Helpers } from './helpers';
import { Vars } from './vars';

@Injectable()
export class Search {

    searchWordInputValue: any = '';  
    
    constructor( private global: GlobalData,
                 private popup: Popup,
                 private helpers: Helpers,
                 private api: ApiService,
                 private vars: Vars) {}

    searchForWord() {

        if( !this.helpers.checkIfNeedGauth() && this.searchWordInputValue ) {
            this.global.isLoading = true;
            this.api.getWords({searchFor: this.searchWordInputValue.trim() });        
        }
    }
    
    onKeySearchWordInput(event: any) {
            var value = event.target.value.trim();
            this.global.currentWordlist = null;
            if (value.length > 0) {
                var isAnyLevelChecked = false;
                this.global.hskLevels.forEach( (l) => {
                    if (l.checked == true) {
                        isAnyLevelChecked = true;
                        return
                    }
                })        
                if (isAnyLevelChecked) {
                    this.global.isLoading = true;
                    this.global.selectedExamples.wordsCounter = 0;
                    this.api.getWords({searchFor: value });
                } else {
                    this.popup.showPopup({ msg: "Please select HSK level" })
                }

            } else if (event.keyCode == '32' 
                    || event.keyCode == '8' 
                    || event.keyCode == '46'
                    || event.keyCode == '13'
                    || event.keyKode == '27') 
            {
                this.global.isLoading = true;
                this.global.selectedExamples.wordsCounter = 0;
                this.api.getWords({});
            }
    }

    clearSearchWordInputValue() {
        this.searchWordInputValue = '';
    }
}