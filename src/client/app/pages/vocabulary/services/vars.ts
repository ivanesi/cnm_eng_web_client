import { Injectable } from '@angular/core';

@Injectable()
export class Vars { 
    isNeedGauth: boolean = false;
    isShowYtPlaylistsList = false;
}
