import { Component} from '@angular/core';
  
@Component({
    selector: 'not-found-app',
    template: `<h3>Sorry, we haven't this page</h3>`
})
export class PageNotFoundComponent { 
    
}
