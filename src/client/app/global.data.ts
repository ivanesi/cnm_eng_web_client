import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { Config } from './app.config';

@Injectable()
export class GlobalData {
      
    
    public googleOauth2Url: string = null;
    
    public userinfo: {
        userinfo: { email:string }
    } = null;

    public ytPlaylists: {
        playlists:{
            items:Array<any>,
            nextPageToken: string
        }        
    } = null; 
    
    public hskLevels: Array<any> = null;

    public words: {
        words:Array<any>,
        total:any
    } = {
        words:null,
        total:0       
    };

    public wordlists: Array<any> = [];

    public selectedExamples: {
        wordsCounter: number,
        exCounter: number,
        ex: Array<any>
    } = {
        wordsCounter: 0,
        exCounter: 0,
        ex: []
    }

    public currentWordlist: string = null;

    public progressBar: {
        isShow: boolean,
        progress: number,
        progressPersent: string,
        finished: boolean,
    } = {
        isShow: false,
        progress: 0,
        progressPersent: "0%",
        finished: false
    }

    public isLoading: boolean = false;

    public isShowYtPlaylistsList: boolean = false;
    
    public addItemsToPlaylistLog: Array<any> = []
    public lastYtPlaylistLink: string = '';

    constructor() {

    }


    defineWordlists() {
        
        this.wordlists = [];

        var array:Array<any> = [];
        var isIn = false;
        
        this.hskLevels.forEach( (l, i) => {
            if (l.checked == true) {
                if (i == 0) {
                    array = l.wordlists.slice()
                } else {
                    l.wordlists.forEach( (wl:any) => {
                        array.forEach( (awl:any) => {
                            if (wl.name == awl.name) {
                                // awl.elCount += wl.elCount //BAG - try check/uncheck selectHskLevelCheckboxes
                                isIn = true;
                            } 
                        })
                        if (!isIn) array.push(wl)
                    })
                }
            }
        })        
        
        

        array = array.sort(sortFoo);
        
        this.wordlists = array.slice();
        
        array = [];

        function sortFoo(a:any, b:any){
            if (a.elCount < b.elCount) return 1
            if (a.elCount > b.elCount) return -1
            return 0
        }
    }

}